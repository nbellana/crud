const mysql = require("mysql");
const prompt = require("prompt-sync")({ sigint: true });

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root123",
  database: "test",
});

const runAQuery = (query) => {
  con.connect((err) => {
    if (err) {
      console.log("Error connecting to Db");
      return;
    }
    console.log("Connection established");
  });

  con.query(query, function (err, result) {
    // console.log(result)
    if (err) throw err;
    console.log("query executed!");
    console.log(result);
  });
};

//Function to create database
const createDB = () => {
  var dbName = prompt("Enter dbname: ");
  return `CREATE DATABASE ${dbName}`;
};

//Function to create table
const createTable = () => {
  const tableName = prompt("Table name : ");
  const noOfColumns = prompt("No of Cols : ");
  query = "";
  for (let i = 0; i < noOfColumns; i++) {
    colName = prompt("Enter colname : ");
    dataType = prompt("Enter datatype : ");
    query += colName + " " + dataType + ",";
  }
  query = query.substring(0, query.length - 1);
  console.log(`CREATE TABLE ${tableName} (${query})`);
  return `CREATE TABLE ${tableName} (${query})`;
};

//Function to insert records into table
const insertRecords = () => {
  const tableName = prompt("Table name: ");
  const noOfColumns = prompt("No of Cols : ");
  columns = "";
  values = "";
  for (let i = 0; i < noOfColumns; i++) {
    colName = prompt(`Enter colname-${i + 1} : `);
    columns += colName + ",";
    const type = prompt("Enter 0-str 1-int datatype : ");
    value = prompt("Enter value : ");
    type == 0 ? (values += "'" + value + "'" + ",") : (values += value + ",");
  }

  columns = columns.substring(0, columns.length - 1);
  values = values.substring(0, values.length - 1);
  console.log(`INSERT INTO ${tableName} (${columns}) VALUES (${values})`);
  return `INSERT INTO ${tableName} (${columns}) VALUES (${values})`;
};

//Function to specify the database to be used
const useDB = () => {
  var dbName = prompt("Enter dbname: ");
  return `use ${dbName}`;
};

//Function to read metadata of the table
const readTable = () => {
  var tableName = prompt("Enter table name: ");
  return `DESC ${tableName}`;
};

//Function to add primary key by altering table
const addPrimaryKey = () => {
  var tableName = prompt("Enter table name: ");
  var constraint = prompt("Enter constraint_name: ");
  var newPrimaryKey = prompt("Enter column name to make it primary key: ");
  return `ALTER TABLE ${tableName} ADD CONSTRAINT ${constraint} PRIMARY KEY (${newPrimaryKey})`;
};

//Function to read records
const readRecords = () => {
  var tableName = prompt("Enter tablename: ");
  return `SELECT * FROM ${tableName}`;
};

//Function to update records
const updateRecords = () => {
  var tableName = prompt("Enter table name: ");
  var updateCol = prompt("Enter table name: ");
  var updateValue = prompt("Enter table name: ");
  var keyCol = prompt("Enter table name: ");
  var keyValue = prompt("Enter table name: ");
  return `UPDATE ${tableName} SET ${updateCol} = '${updateValue}' WHERE ${keyCol} = ${keyValue}`;
};

//Function to delete records
const deleteRecords = () => {
  var tableName = prompt("Enter table name: ");
  var keyCol = prompt("Enter table name: ");
  var keyValue = prompt("Enter table name: ");
  return `DELETE FROM ${tableName} WHERE ${keyCol}='${keyValue}'`;
};

//Function to delete table
const deleteTable = () => {
  var tableName = prompt("Enter table name: ");
  return `Delete TABLE ${tableName}`;
};

//Function to delete database
const deleteDB = () => {
  var dbName = prompt("Enter database name: ");
  return `DROP DATABASE ${dbName}`;
};
// var sql = ``;
const runQuery = (x) => {
  switch (x) {
    case 1:
      //Create database
      runAQuery(createDB());
      break;

    case 2:
      //Use database
      runAQuery(useDB());
      break;

    case 3:
      //Create table
      runAQuery(createTable());
      break;

    case 4:
      //Read table
      runAQuery(readTable());
      break;

    case 5:
      //Update table
      runAQuery(addPrimaryKey());
      break;

    case 6:
      //Create records
      runAQuery(insertRecords());
      break;

    case 7:
      //Read records
      runAQuery(readRecords());
      break;

    case 8:
      //Update records
      runAQuery(updateRecords());
      break;

    case 9:
      //Delete records
      runAQuery(deleteRecords());
      break;

    case 10:
      //Delete table
      runAQuery(deleteTable());
      break;

    case 11:
      //Delete database
      runAQuery(deleteDB());
      break;
  }
};

var CaseNo = 7;
runQuery(CaseNo);

con.end((err) => {});
