-- create database
create database neelimadb;

-- read database
use neelimadb;

-- update database
"rename database neelimadb to mydb";
 
-- create table 
"create table bookstore(
item_name VARCHAR(50),
id int(10), 
price int(10),
quantity int(10))";

-- read table
desc bookstore;

-- update table
ALTER TABLE bookstore ADD CONSTRAINT BS_KEY PRIMARY KEY (id)

-- create records
"insert into bookstore values("Pen", 1, 50, 100);"
"insert into bookstore values("Pencil", 2, 10, 200);"
"insert into bookstore values("Eraser", 3, 5, 80);"
"insert into bookstore values("Papers", 4, 2, 500);"
"insert into bookstore values("Sharpner", 5, 10, 70);"
  
-- read record
SELECT * FROM bookstore;

-- update record
"UPDATE bookstore SET price = 20  WHERE id = 1";

-- delete record
"DELETE FROM bookstore WHERE id=1";

-- delete table
drop table bookstore;

-- delete database
 DROP DATABASE neelimadb;


